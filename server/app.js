// TODO: 1. Load/import the module required by this application

// TODO: 2. Invoke an instance of express and assign it to a variable called app

/* Defines server port.
 Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
 */
const NODE_PORT = process.env.NODE_PORT || 3000;

// A function that handles requests and sends a response
// Since no path was specified, this function would handle all client requests
app.use(function (req, res) {
    res.send('It Works!! Path Hit: ' + req.originalUrl);
});

// Starts the server on localhost (default)
app.listen(NODE_PORT, function () {
    console.log('Server listening on: http://localhost:%s', NODE_PORT);
});